# Codebase release 0.4 for pyBumpHunter

by Louis Vaslin, Samuel Calvet, Vincent Barra, Julien Donini

SciPost Phys. Codebases 15-r0.4 (2023) - published 2023-07-14

[DOI:10.21468/SciPostPhysCodeb.15-r0.4](https://doi.org/10.21468/SciPostPhysCodeb.15-r0.4)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.15-r0.4) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Louis Vaslin, Samuel Calvet, Vincent Barra, Julien Donini

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Live (external) repository at [https://github.com/lovaslin/pyBH-test/tree/a859c15694de6a76a7c7e19d680fb9ed6e9022e4](https://github.com/lovaslin/pyBH-test/tree/a859c15694de6a76a7c7e19d680fb9ed6e9022e4)
* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.15-r0.4](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.15-r0.4)
